### Overview

This document details steps necessary to run the connector app on a Windows system.  This has never
been tested in a live environment, but it has been debugged at a high level on Windows 10.  It is
assumed that the Windows system has access to all SP packets (eg port mirroring).

### Configure Windows to Host Nodejs App

Install [node](https://nodejs.org/en/download/current/) version 7.6 or better (need async await
support).

Install [git](https://git-scm.com/download/win).

Install [wireshark](https://www.wireshark.org/download.html).

### Configure Connector app on Windows

Type the following in a Git Bash session:

```
git clone https://gitlab.com/JJNorcal/sp-pvo-con.git
cd sp-pvo-con
cd windows
d=`pwd`
service-wrapper -install "$d/winstart.bat" 
```

Next, review and modify both files in the config direcgtory to match your situation.

At this point, you should be set.  You can launch the connector app by invoking
"sp-pvo-con/windows/winstart", or you could start your new service by invoking "net start
sp-pvo-con".  The service will also start automatically after a windows reboot.
