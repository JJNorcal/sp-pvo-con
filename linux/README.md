### Overview

This document focusses on a Raspberry Pi based node app, though it is possible to host the app on
any computer which has access to the SP monitor packets.  If you already have a Linux machine set
up, you can skip to [Configure Appliance to Host Nodejs
App](configure-appliance-to-host-nodejs-app).

This solution requires no appliance ethernet configuration, but note that there is an assumption the
appliance will be provisioned by a local DHCP server.  The app supports reserve SP IP address
reservation which reduces the number of packets captured and logged.

The capacity of the SD card I used to build the appliance is 32GB.  Turns out that 4GB will more
than suffice, and while I have not rebuilt using a 4GB card, I have downsized the partition to
reduce excessive pi boot time that occurs after a hard power cycle which results from file system
check on the way up.  My experience with a 4GB partion has been outstanding, with hard power cycles
resulting in boots in less than 2 minutes.  Probably best to start with a 4GB SD.  You can start
with a larger card and repartion, but note this operation can only be done when the SD is unmounted.
I was able to boot to a GParted Live USB stick on my laptop and then shrink the SD partion.

I originally intended to bridge the wireless interface in the Pi 3 to its one and only ethernet
port.  I was unable to bridge the wlan0 interface (seems to be a limitation in the linux wifi
driver), so I connected a USB ethernet adapter and bridged it to the Pi 3 enet port.

The appliance requires a DHCP IP addresses for the bridge port (shared by all bridged ports).  These
instructions will refer to another DHCP IP for the wlan port, which reprents my ssh target.  Turns
out that, while the second IP is associated with the wlan MAC address, the ssh path actually takes
place over the faster wired network.  I ran across reference material indicating that Linux bridging
takes place using the numerically smallest MAC address.  If this is true, then a single IP address
can be reserved by provisioning the smallest wired MAC address.

I discovered rather late in the process that my system is connected by powerline AND wifi, with
powerline having higher priority.  I discovered this when my powerline adapter got flaky, and as a
result, I added a second USB enet adapter and tapped both the powerline output as well as wifi
output.  I'm going to leave the appliance creation procedures to refer to a two port switch, but
adding eth2 in the interfaces and dhcpcd.conf files is straightforward.

I am not a Linux networking wizard.  These procedures worked fine for me, but there might be a better
way.

You will need to be able to edit text files in Linux.  I use vim, but beginners should stick with
nano.

### Appliance Requirements

Below are the components of the pi based network appliance.  The links in parens indicate the
products that I used.

1. Raspberry Pi 3 (https://www.amazon.com/gp/product/B01DMFQZXK/)
2. USB network adapter (https://www.amazon.com/gp/product/B00PC0H9IE/)
3. Micro SD card (https://www.amazon.com/gp/product/B010Q57T02/)

### Procedural Overview

1. Build Pi Network Appliance
2. Configure Appliance to Host Nodejs App
3. Configure Connector app

### Build Pi Network Appliance

The following are headless Debian installation procedures.  I did this from a Windows 10 bash console
(http://www.windowscentral.com/how-install-bash-shell-command-line-windows-10), but you could equally
well use putty.  There are many headed and headless Debian installation guides available on the
Internet.

1.  [Download and unzip image](http://downloads.raspberrypi.org/raspbian_latest).
2.  [Download and install image writer](https://www.raspberrypi.org/documentation/installation/installing-images/README.md).
3.  Write image to SD card.
4.  Create empty file named "ssh" in SD drive (e.g. create "e:\ssh").
5.  Plug in ethernet cable and boot pi
6.  Log onto home router to determine IP address supplied via DHCP
7.  establsh ssh session (e.g. "ssh pi@xxx.xxx.xxx.xxx", password is "raspberry") and type the following commands:

```
sudo su  
apt-get upgrade  
apt-get update  
apt-get install -y vim bridge-utils
```

Append the following to the end of /etc/wpa_supplicant/wpa_supplicant.conf:  

```
network={
    ssid="YourCaseSensitiveSSID"
    psk="YourWifiPassword"
}
```

Add following line to the end of /etc/dhcpcd.conf:

```
denyinterfaces eth0 eth1 br0
```

Replace all of the lines after the "source-directory" line in /etc/network/interfaces with the
following:

```
auto lo
iface lo inet loopback

allow-hotplug eth0
iface eth0 inet manual

allow-hotplug eth1
iface eth1 inet manual

allow-hotplug wlan0
iface wlan0 inet manual
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

allow-hotplug wlan1
iface wlan1 inet manual
    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

auto br0
iface br0 inet dhcp
    bridge_ports eth0 eth1
    bridge_stp off
    bridge_fd 0
    bridge_maxwait 0
```

At this point, you can reboot and you should have a functional enet switch.  You can test it by
putting it between two existing devices on your home network.  The device will silently forward
packets between the two enet jacks as needed.  To reboot, type "reboot".

### Configure Appliance to Host Nodejs App

Establish an ssh session as per the last section and type the following commands:

```
sudo su
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nodejs
apt-get install -y tshark
apt-get install -y git
```

Installing tshark will throw up some GUI.  Specify that you must not be admin to run tshark.  A
wireshark group will be created, and then you need to add a user to the group.  I am still using pi
for default login.  So:

```
usermod -a -G wireshark pi

Privilege GUI setting can be changed with:

dpkg-reconfigure wireshark-common
```

### Configure Connector app on Linux

Type the following in an ssh session:

```
sudo su
git clone https://gitlab.com/JJNorcal/sp-pvo-con.git
cd sp-mon-con
npm install
```

Next, review and modify the files in the config directory.  They should be self explanatory.

At this point, you should be set.  You can launch the connector app by typing "./start-con" from
your sp-mon-con/linux directory.  If your appliance is between your SP monitoring system and the Internet,
then you should see a report every 5 minutes.  If all is working, it will look like this:

```
04/23/2017 14:32:19 success: 20170423 14:20 6258
04/23/2017 14:38:42 success: 20170423 14:25 5942
04/23/2017 14:43:42 success: 20170423 14:30 6356
```

The last step is to configure your appliance to automatically launch the app on startup.  To do this,
you need to copy the sp-pvo-con file from your sp-pvo-con/linux directory to the linux startup directory.
Assuming you just estalbished an ssh session:

```
cd sp-pvo-con/linux
cp sp-pvo-con /etc/init.d
```

The sp-pvo-con file you just copied assumes that you are logging in as pi.  If you have changed
this, then search for and modify SCRIPT and RUNAS.

Now you can reboot, ssh in, and confirm that the app has been automatically started by typing
"ps a | grep node".  You should see (the numbers in the first column correspond to process ID and can
be different on each boot).:

```
 1080 pts/0    Sl     0:03 /usr/bin/node /home/pi/sp-pvo-con/app/main.js
 1239 pts/0    S+     0:00 grep node
```
