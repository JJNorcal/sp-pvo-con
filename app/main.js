"use strict";

var credentials     = require("../config/credentials.js");
var control         = require("../config/control.js");
var utils           = require("./utils.js");
var persistentArray = require("./persistent-array");
var request         = require("request");

require("./logger.js");

// asynchronous wrapper to promisize request
// request-promise package has dependency issues
function requestAsync(httpMessage) {

    return new Promise(function(resolve, reject) {
        request(httpMessage, function(error, response, body) {
            try {
                if (error) {
                    console.log(error);
                    reject(error);
                } else {
                    resolve(body);
                }
            }
            catch (err) {
                reject(err);
            }
        });
    });
}

// synchronous http request wrapper
async function sendRequest(httpMessage) {

    if (! control.sendToPvo) {
        return "OK 200: Added Status";
    }

    var retVal = null;

    await requestAsync(httpMessage)
    .then(
        function (body) {
            retVal = body;
        },
        function (error) {
            console.log(error);
            retVal = null;
        }
    );

    return retVal;
}

function retryEntry(date, time, power, pvoPost) {
    this.date = date;
    this.time = time;
    this.power = power;
    this.post = pvoPost;
}

// global variables
var retryQueue = new persistentArray.array("./retry.dat");
var retryTimer = null;
var retryTimeout = 1000 * 2;

// main
(function main() {

    var tshark;

    // tcp byte stream buffer
    var lastTime = null;
    var lastLifetimeEnergy = "";

    var spawn = require("child_process").spawn;

    // spawn tshark:
    //	 "tcp port 80 ..." => limit capture to http on port 80
    //   -v 		   => tcp data starts with a tab
    //   -1 		   => line buffered
    // tshark -i br0 -f "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)
    // && (host 172.23.25.10 or host 172.23.25.11)" -T fields -e text
    {
        var filter = "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)";

        if (control.spIpAddresses.length > 0) {

            filter += " and (src host " + control.spIpAddresses[0];

            control.spIpAddresses.shift();

            for (var i = 0; i < control.spIpAddresses.length; ++i) {
                filter += " or src host " + control.spIpAddresses[i];
            }

            filter += ")";
        }

        var params = [
            "-l",
            "-T", "fields", "-e", "text",
            "-f", filter
        ];

        if (control.captureIface != "") {
            params.push("-i", control.captureIface);
        }

        if (control.debug != undefined) {

            if (control.debug.useTcpDump) {
                params.push("-r", control.debug.tcpDumpFile);
            }

            else if (control.debug.saveTcpDump) {
                params.push("-w", control.debug.tcpDumpFile);
            }
        }

        var command = control.tsharkPathname != "" ? control.tsharkPathname : "tshark";

        tshark = spawn(command, params);
    }

    if (retryQueue.length > 0) {
        retryTimer = setTimeout(retryTimeoutRoutine, retryTimeout);
    }

    // tshark stdout stream
    tshark.stdout.on("data", async function (data) {

        data = data.toString();

        if (control.debug.logPackets != undefined && control.debug.logPackets) {
            console.log(data.replace(/\n$/, ""));
        }

        if (! /,post[^,]*,[^,]*,[^,]*,([\s\S]*)/i.test(data)) {
            return
        }

        var message = RegExp.$1.replace(/\\t/g, " ").replace(/\\r/g, "").replace(/\\n|,/g, "\n");

        var result;
        var regExp = /^([\S]*) (.*)$/mg;

        // loop through each line in the response
        while ((result = regExp.exec(message)) != null) {

            if (control.logAllSpData && ! /POST.*SMS2DataCollector|HTTP request|GeoIP: Unknown/m.test(result[0])) {
                console.log(result[0]);
            }

            // compare to the almighty 130!
            if (result[1] != "130") {
                continue;
            }

            var regExp2 = /([\S]+) +[\S]+ +[\S]+ +([\S]+) +([\S]+) +[\S]+ +[\S]+ +[\S]+ +[\S]+ +[\S]+ +[\S]+ *$/;

            if (!regExp2.test(result[2])) {
                continue;
            }

            var timestamp = RegExp.$1;
            var currentLifetimeEnergy = RegExp.$2;
            var currentPower = RegExp.$3;

            /(....)(..)(..)(..)(..)(..)/.test(timestamp);

            var year = RegExp.$1 + "-" + RegExp.$2 + "-" + RegExp.$3;
            var time = RegExp.$4 + ":" + RegExp.$5 + ":" + RegExp.$6;
            var dateObj = new Date(year + "T" + time + "Z");

            // SP occasionally reports the same data point multiple times
            if (lastTime != null && dateObj.getTime() == lastTime.getTime()) {
                continue;
            }

            const fiveMin = 5 * 60 * 1000;

            // check for missing data points
            if (   lastTime != null
                && dateObj.getDate() == lastTime.getDate()
                && dateObj - lastTime > fiveMin
               ) {

                // one or more data points went unreported

                var unaccountedEnergy =

                                // difference in lifetime reports
                                (currentLifetimeEnergy - lastLifetimeEnergy)

                                // energy from most recent report
                                - (currentPower * (5 / 60));

                if (unaccountedEnergy < 0) {
                    unaccountedEnergy = 0;
                }

                var missingDataPoints = ((dateObj - lastTime) / fiveMin) - 1;
                var missedIntervalEnergy = unaccountedEnergy / missingDataPoints;
                var missedIntervalPower = Math.round(missedIntervalEnergy / (5/60) * 1000);

                lastTime = new Date(lastTime.getTime() + fiveMin);

                console.log("accounting for %s kWh over %d minutes starting at %s:%s",
                            unaccountedEnergy.toFixed(5),
                            missingDataPoints * 5,
                            utils.zeroPad(lastTime.getHours(), 2),
                            utils.zeroPad(lastTime.getMinutes(), 2)
                           );

                // spread missedIntervalEnergy over missingDataPoints
                do {
                    await commitData(lastTime, missedIntervalPower);
                    lastTime = new Date(lastTime.getTime() + fiveMin);
                } while (dateObj - lastTime > 0);
            }

            // process new data point
            await commitData(dateObj, Math.round(parseFloat(currentPower) * 1000));

            lastLifetimeEnergy = currentLifetimeEnergy;
            lastTime = dateObj;
        }
    });

    tshark.stderr.on("data", function (data) {
        console.error(data.toString());
    });

    tshark.on("exit", function (code) {
        console.error("tshark exited with code " + code.toString());
    });

})();

async function commitData(time, power) {

    var year = time.getFullYear().toString();
    var month = utils.zeroPad(time.getMonth() + 1, 2);
    var day = utils.zeroPad(time.getDate(), 2);
    var date = year + month + day;

    var hours = utils.zeroPad(time.getHours(), 2);
    var minutes = utils.zeroPad(time.getMinutes(), 2);
    time = hours + ":" + minutes;

    // pvo post template
    var pvoPost = {
        url: "https://pvoutput.org/service/r2/addstatus.jsp"
                + "?d=" + date
                + "&t=" + time
                + "&v2=" + power.toString() + "\n\n",
        method: "POST",
        headers: {
            "Host": "pvoutput.org",
            "X-Pvoutput-Apikey": credentials.pvo.apiKey,
            "X-Pvoutput-SystemId": credentials.pvo.systemId,
            "Accept": "*/*"
        },
        body: ""
    };

    // supply parameters in URL

    var response = null;

    // process retry queue
    if (
           //push onto queue if not empty
           (retryQueue.length > 0)

            // push onto queue if the send fails
        || ((response = await sendRequest(pvoPost)) == null)

            // handle pvo failures
        ||  (! /^\s*OK\s+200\D/i.test(response))
       )
    {
        if (response != null) {

            console.log("request: %j, response: %j", pvoPost, response);    // failure

            if (! /Service Unavailable/i.test(response)) {
                return;
            }
        }

        retryTimeout = 1000 * 60 * 5;

        retryQueue.push(new retryEntry(date, time, power, pvoPost));

        if (retryTimer == null) {
            retryTimer = setTimeout(retryTimeoutRoutine, retryTimeout);
        }

        return;
    }

    // success logging
    console.log("success: " + date + " " + time + " " + power);     // success
}

async function retryTimeoutRoutine() {

    var response;

    console.log("retry attempt, queue length = " + retryQueue.length.toString());

    var retry = retryQueue.element(0);

    if ((response = await sendRequest(retry.post)) != null) {

        if (/^\s*OK\s+200\D/i.test(response)) {

            console.log("success: " + retry.date + " " + retry.time + " " + retry.power);     // success

            retryTimeout = 1000 * 2;

            retryQueue.shift();
        }

        else {
            console.log("request: %j, response: %j", retry.post, response);    // failure
        }
    }

    if (retryQueue.length > 0) {
        retryTimer = setTimeout(retryTimeoutRoutine, retryTimeout);
    }

    else {
        retryTimer = null;
    }
}
