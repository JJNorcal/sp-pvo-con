"use strict";

const fs = require("fs");

exports.array = function(pathName) {

    var headerObj = { length: 0 };
    var headerString = "";

    var fd = fs.openSync(pathName, "a+");

    if (fd == -1) return null;

    this.pathName = pathName;
    this.fileSize = fs.statSync(pathName).size;
    this.length = 0;

    var position;
    const maxHeaderSize = 32;
    var buffer = new Buffer(maxHeaderSize);
    var bufString = "";

    for (position = 0; position < this.fileSize; position += headerString.length + headerObj.length) {

        fs.readSync(fd, buffer, 0, maxHeaderSize, position);

        bufString = buffer.toString();

        /([^\}]*\}).*/.test(bufString);
        headerString = RegExp.$1;
        headerObj = JSON.parse(headerString);

        ++this.length;
    }

    fs.closeSync(fd);

    if (this.length == 0) {
        fs.unlinkSync(pathName);
    }

    this.push = function (obj) {

        fd = fs.openSync(this.pathName, "a+");

        var jsonString = JSON.stringify(obj);

        headerObj.length = jsonString.length;

        buffer = new Buffer(JSON.stringify(headerObj));

        fs.writeSync(fd, buffer, 0, buffer.length, this.fileSize);
        this.fileSize += buffer.length;

        buffer = new Buffer(jsonString);

        fs.writeSync(fd, buffer, 0, buffer.length, this.fileSize);
        this.fileSize += buffer.length;

        ++this.length;

        fs.closeSync(fd);
    };

    this.shift = function () {

        if (this.length == 0) return;

        fd = fs.openSync(this.pathName, "r+");

        buffer = new Buffer(maxHeaderSize);

        fs.readSync(fd, buffer, 0, maxHeaderSize, 0);

        bufString = buffer.toString();

        /([^\}]*\}).*/.test(bufString);
        headerString = RegExp.$1;
        headerObj = JSON.parse(headerString);

        var entryLength = headerObj.length + headerString.length;
        var newFileLength = this.fileSize - entryLength;

        buffer = new Buffer(newFileLength);

        fs.readSync(fd, buffer, 0, newFileLength, entryLength);
        fs.writeSync(fd, buffer, 0, newFileLength, 0);
        fs.ftruncateSync(fd, newFileLength);

        this.fileSize = newFileLength;

        --this.length;

        fs.closeSync(fd);

        if (this.length == 0) {
            fs.unlinkSync(this.pathName);
        }
    };

    this.element = function(elementNumber) {

        var fd = fs.openSync(this.pathName, "r+");

        buffer = new Buffer(maxHeaderSize);

        var obj = null;

        for (position = 0; position < this.fileSize; position += headerString.length + headerObj.length) {

            fs.readSync(fd, buffer, 0, maxHeaderSize, position);

            bufString = buffer.toString();

            /([^\}]*\}).*/.test(bufString);
            headerString = RegExp.$1;
            headerObj = JSON.parse(headerString);

            if (elementNumber-- > 0) {
                continue;
            }

            buffer = new Buffer(headerObj.length);

            fs.readSync(fd, buffer, 0, headerObj.length, position + headerString.length);

            bufString = buffer.toString();

            obj = JSON.parse(bufString);

            break;
        }

        fs.closeSync(fd);

        return obj;
    };

    return this;
};
