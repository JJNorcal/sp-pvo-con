"use strict";

var control = require("../config/control.js");

var util = require("util"),
    winston = require("winston"),
    logger = new winston.Logger(),
    utils = require("./utils.js");

require("winston-daily-rotate-file");

// Override the built-in console methods with winston hooks
if (control.logging.toFile) {
    logger.add(winston.transports.DailyRotateFile, {
        filename: control.logging.filename,
        handleExceptions: true,
        exitOnError: false,
        level: "info",
        maxsize: 10 * 1024 * 1024,
        maxFiles: 2,
        json: false,
        timestamp: getTimeStamp
    });
}

if (control.logging.toConsole) {
    logger.add(winston.transports.Console, {
        colorize: true,
        timestamp: getTimeStamp,
        level: "info"
    });
}

function formatArgs(args){
    return [util.format.apply(util.format, Array.prototype.slice.call(args))];
}

console.log = function(){
    logger.info.apply(logger, formatArgs(arguments));
};

console.info = function(){
    logger.info.apply(logger, formatArgs(arguments));
};

console.warn = function(){
    logger.warn.apply(logger, formatArgs(arguments));
};

console.error = function(){
    logger.error.apply(logger, formatArgs(arguments));
};

console.debug = function(){
    logger.debug.apply(logger, formatArgs(arguments));
};

function getTimeStamp() {

    var monthString = "Jan 1 Feb 2 Mar 3 Apr 4 May 5 Jun 6 Jul 7 Aug 8 Sep 9 Oct 10 Nov 11 Dec 12";

    // Sat Apr 22 2017 09:13:28 GMT-0700 (PDT)
    /\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)\s*(\S*)/.test(Date().toLocaleString());

    var mon = RegExp.$2;
    var day = utils.zeroPad(RegExp.$3, 2);
    var year = RegExp.$4;
    var time = RegExp.$5;

    var regExp = new RegExp(mon + "\\s(\\S*)", "i");

    regExp.test(monthString);

    mon = utils.zeroPad(RegExp.$1, 2);

    return mon + "/" + day + "/" + year + " " + time;
}
