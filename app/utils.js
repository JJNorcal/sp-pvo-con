"use strict";

module.exports = {

    zeroPad: function (number, numDigits) {

        const template = "0000000000000000";

        if (typeof number == "number") {
            number = number.toString();
        }

        while (numDigits > number.length) {
            number = template + number;
        }

        return(number.slice(number.length - numDigits));
    }
};
