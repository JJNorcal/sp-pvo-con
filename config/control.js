"use strict";

module.exports = {

    // false for testing purposes
    sendToPvo       : true,

    // true to log all SP data
    logAllSpData    : false,

    // put as many SP IP addresses heare as you have (lan, wifi, powerline)
    spIpAddresses   : [ "172.23.25.10", "172.23.25.11" ],

    // leave blank if tshark executable location is in $PATH
    tsharkPathname  : "",

    // interface (-i param) passed to tshark
    captureIface    : "br0",

    logging: {

        // true logs to console (for debugging)
        toConsole   : false,

        // true logs to file
        toFile      : true,

        // log file (will rotate daily)
        filename    : "/home/pi/sp-pvo-con/logs/log"
    },

    debug: {
        useTcpDump  : false,
        saveTcpDump : true,
        tcpDumpFile : "/home/pi/sp-pvo-con/logs/dump.pcap",
        logPackets  : false
    }
}
