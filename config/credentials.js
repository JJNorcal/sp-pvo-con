module.exports = {

    // mandatory pvo credentials
    pvo: {
        apiKey   : "my-pvo-api-key",
        systemId : "my-pvo-system-id"
    },

    // sunpower credentials (currently unused)
    sp: {
        password :"MySunPowerPassword",    // edit-me
        username :"MySunPowerUsername"     // edit-me
    }
}
