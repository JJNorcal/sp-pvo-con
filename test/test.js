var assert = require("chai").assert;

describe("persistent arrays", function () {

    var header = { length: 0 };
    var headerString;
    var obj;
    var exists;

    const fs = require("fs");

    var pathName = "./testArray.dat";

    if (fs.existsSync(pathName)) {
        fs.unlinkSync(pathName);
    }

    var persistentArray = require("../app/persistent-array");
    var pArray = new persistentArray.array(pathName);

    it("instantiate array", function () {

        var exists = fs.existsSync(pathName);

        assert(!exists, "file should not exist");
        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });

    var obj1 = new testObj("a string", 1);

    it("push object", function () {

        header.length = JSON.stringify(obj1).length;

        headerString = JSON.stringify(header);

        pArray.push(obj1);

        assert(pArray.length == 1, "array length should be 1");
        assert(pArray.fileSize == header.length + headerString.length);
    });

    it("examine element", function () {

        obj = pArray.element(0);

        assert(obj.stringVal == obj1.stringVal, "string examined should match string pushed");
        assert(obj.numVal == obj1.numVal, "number examined should match number pushed");
    });

    it("shift array", function () {

        pArray.shift();

        exists = fs.existsSync(pathName);

        assert(!exists, "file should be deleted");

        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });

    it("series of operations", function () {

        var testObjects = [
            new testObj("short string", 9),
            new testObj("a longer string", 123456789),
            new testObj("mary had a little lamb whose fleece was white as snow", 1234567891234567889)
        ];

        var i;

        for (i = 0; i < testObjects.length; ++i) {
            pArray.push(testObjects[i]);
        }

        for (i = 0; i < testObjects.length; ++i) {

            obj = pArray.element(0);

            assert(obj.stringVal == testObjects[i].stringVal, "string examined should match string pushed");
            assert(obj.numVal == testObjects[i].numVal, "number examined should match number pushed");

            pArray.shift();
        }

        exists = fs.existsSync(pathName);

        assert(!exists, "file should be deleted");

        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });

});

function testObj(stringVal, numVal) {
    this.stringVal = stringVal;
    this.numVal = numVal;
}
