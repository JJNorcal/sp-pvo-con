### Intro

This project maintains a node app which implements a SunPower (SP) to PVOutput connector.  The
initial project focussed exclusively on a Raspberry Pi based appliance which hosts the app while
acting as a network switch, thus eliminating the need for port mirroring.  Much of this document
represents notes detailing the generation of the pi appliance.

Debugging the app on Windows proved to be useful.  Because the reach was relatively low, Windows
support was added which presumes a Windows server that has access to all SP packets (eg port
mirroring).  The Windows solution has been debugged at a high level, but it has never been tested on
a live network with access to SP packets.

The packet capture method used here was discovered by a solarpaneltalk member.  You can read more
about two different options
[here](https://www.solarpaneltalk.com/forum/solar-panels-for-home/solar-panel-system-equipment/19587-mirroring-intercepting-sunpower-monitoring-traffic/page6).

### Caveats

Buyer beware!  I will make an effort to respond to reasonable questions, but this package should be
considered basically unsupported.

I am not an expert on SP monitoring systems.  I assume that they have manufactured many different
models over the years, and I do not know if all of them follow the same packet format.  I have a
[PVS5x](https://fccid.io/document.php?id=2725912).  If you have a different
model, then you would need to set up a sniffer to confirm compatible packets.

My understanding is that there are multiple network options built into SP monitoring system.  Mine
was installed using a powerline adapter, so I can insert my pi appliance between it and my home
router.  If yours was set up using wifi, then you would need to insert the appliance between the
wired port of your access point and your Internet router.  If your access point is your Internet
router, then you would probably need to get another access point and insert the appliance on the
wired ports between the two APs.

This approach is currently reliant upon the fact that SP is not using SSL (HTTPS), which is a bit
surprising.  Should SP convert in the future, then work will be required to accommodate.

[Linux documentation](./linux/README.md) describes creation of a Raspberry Pi appliance image along
with installation of the connector app running as a Linux service.

[Windows documentation](./windows/README.md) describes installation of the connector app running as
a Windows service.
